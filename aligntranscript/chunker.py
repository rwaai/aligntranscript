#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals


import logging
from itertools import groupby
import numpy as np

from config import CFG

from audio import Chunk

#FIXME: this module is inelegant -- refactor one day



class ChunkError(ValueError):
    pass

#a chunker.Chunking is a sequence of audio.Chunks -- it has a perc and a thr
#  but knows nothing about transcription
#an alignment.Alignment is a pairing zip(chunking, rel_durations=None)
#  dp-aligned
#  still knows nothing about transcription(input is rel_durations)
#  it has a cost

class Chunking(object):
    def __init__(self, chunks, perc, thr, times_rel):
        self.chunks = chunks
        self.perc = perc
        self.thr = thr
        self.times_rel = times_rel

    def __str__(self):
        return "<Chunking at percentile {:d}, threshold {:.2e}: {:d} chunks >".format(
            self.perc, self.thr, len(self.chunks))

    def __len__(self):
        return len(self.chunks)

    def __repr__(self):
        return self.__str__()

    @classmethod
    def singleton(cls, chunk):
        return cls([chunk], 0, 0.0, [1.0])

class Chunker(object):
    def __init__(self, audio):
        self.audio = audio
        self.intensity = compute_intensity(audio)

    def get_chunking(self, perc):
        a = self.audio
        threshold = np.percentile(self.intensity, perc)
        silences = np.where(self.intensity < threshold, True, False)
        silences = [(k, len(list(v))) for (k, v) in groupby(list(silences))]
        silences = filter_silences(silences, a.sf)
        try:
            (times_rel, times_cum) = compute_times(silences, a.sf)
        except ChunkError:
            return None
        logging.debug("computed silences for percentile {} ({} chunks, thr={})".format(
            perc, len(times_rel), threshold))
        chunks = []
        for (i, (fromsec, tosec)) in enumerate(times_cum):
            lpause_start = times_cum[i-1][1] if 0 <= i-1 < len(times_cum) else 0.0
            rpause_end   = times_cum[i+1][0] if 0 <= i+1 < len(times_cum) else a.tosec
            c = Chunk(a.data, a.sf,
                       fromsec + a.fromsec,
                       tosec + a.fromsec,
                       lpause_start=lpause_start + a.fromsec,
                       rpause_end=rpause_end + a.fromsec)
            chunks.append(c)
        logging.debug("computed chunking for perc={} (thr={}, {} audio chunks)".format(perc, threshold, len(chunks)))
        return Chunking(chunks, perc, threshold, times_rel)


def compute_intensity(audio, min_pitch=CFG.pause_intensity_min_pitch):
    """
    Compute sound intensity according to the formula used by praat;
    see
    http://uk.groups.yahoo.com/group/praat-users/message/2936
    http://www.fon.hum.uva.nl/praat/manual/Sound__To_Intensity___.html
    """
    logging.info("computing intensity...")
    points = int(audio.sf * 3.2/min_pitch)
    data_sq = np.power(audio.data, 2)
    w = np.kaiser(points, 20.0)
    intensity = np.convolve(data_sq, w/w.sum(), 'valid')
    logging.info("computed intensity for sound (size={}, sf={})".format(
                                               audio.data.size, audio.sf))
    return intensity


def cum_sum_snd(xs):
    total = 0
    for (fst, x) in xs:
        total += x
        yield (fst, total)

def compute_times(silences, sampling_freq, offset=0.0):
    silences_cum = list(cum_sum_snd(silences))
    assert len(silences) == len(silences_cum), str((len(silences), len(silences_cum)))
    samples = [[x[1] for x in silences[i+0:i+2]] for i in range(0, len(silences), 2)]
    samples_cum = [[x[1] for x in silences_cum[i+0:i+2]] for i in range(0, len(silences_cum), 2)]
    sf = float(sampling_freq)
    times = [(p/sf, s/sf) for (p, s) in samples] #times given absolute (secs)
    times_cum = [(p/sf, s/sf) for (p, s) in samples_cum]
    all_sounds = sum(sound_dur for (pause_dur, sound_dur) in times)
    all_pauses = sum(pause_dur for (pause_dur, sound_dur) in times)
    if all_sounds == 0:
        raise ChunkError("no sounds")
    if all_pauses == 0:
        raise ChunkError("no pauses")
    times_rel = [(pause/all_pauses, sound/all_sounds) for (pause, sound) in times]
    logging.debug("#p,s,pc,sc = pause, sound, pause cum, sound cum; given for samples, times, reltimes")
    for i in range(len(samples)):
        logging.debug("[samples: p{:6d} s{:6d}/pc{:8d} sc{:8d}]    "
              "[times: p{:4.3f} s{:4.3f}/pc{:6.3f} sc{:6.3f}]    "
              "[reltimes: p{:3.3f} s{:3.3f}]".format(
            samples[i][0], samples[i][1], samples_cum[i][0], samples_cum[i][1],
            times[i][0], times[i][1], times_cum[i][0], times_cum[i][1],
            times_rel[i][0], times_rel[i][1]))
    times_cum = [(p+offset, s+ offset) for (p,s) in times_cum]
    return (times_rel, times_cum)


def filter_silences(silences,
                   sampling_freq,
                   min_pause=CFG.pause_min_ps_duration,
                   min_sound=CFG.pause_min_sd_duration):
    eps = 0.001
    min_pause = min_pause or eps
    min_sound = min_sound or eps
    def shortest_first(i_pause_spls):
        (_i, (is_pause, spls)) = i_pause_spls
        return spls / (sampling_freq * (min_pause if is_pause else min_sound))
    sil_sorts = sorted(enumerate(silences), key=shortest_first)
    silences_d = dict(sil_sorts)
    segs = len(sil_sorts)
    for (i, (is_pause, spl)) in sil_sorts:
        if shortest_first((i, (is_pause, spl))) > 1:
            break
        if i not in silences_d:
            continue #already removed
        if min_pause <= eps and is_pause:             #!! what does this mean
            continue #do not filter short pauses
        if min_sound <= eps and not is_pause:
            continue #do not filter short sounds

        lefti = i-1
        while lefti >= 0 and lefti not in silences_d:
            lefti -= 1
        (_left, lspl) = silences_d.pop(lefti, (not is_pause, 0))

        righti = i+1
        while righti < segs and righti not in silences_d:
            righti += 1
        (_right, rspl) = silences_d.pop(righti, (not is_pause, 0))

        silences_d[i] = (not is_pause, lspl + spl + rspl)
        #tot_spls = sum(v for (k,v) in silences_d.values())
        #print(len(silences_d), lspl, spl, rspl,  lefti, i, righti, tot_spls)

    sils = [silences_d[i] for i in sorted(silences_d)]
    if not sils[0][0]:
        sils.insert(0, (True, 0))
    if sils[-1][0]:
        sils.append((False, 0))
    #guaranteed to yield full pairs (pausedur, sounddur)
    #the first pause will have duration zero if startswith sound (can happen
    #especially for a very short sound)
    #the last sound will have duration zero if endswith pause (likely)
    return sils

