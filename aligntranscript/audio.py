#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals


import logging, sys

from config import CFG

import soundio

class Audio(object):
    def __init__(self, data, sampling_freq, fromsec=0.0, tosec=None):
        self.sf = sampling_freq
        self.fromsec = fromsec
        self.tosec = len(data)/sampling_freq if tosec is None else tosec
        self.duration = self.tosec-self.fromsec
        self.fromspl = int(sampling_freq*self.fromsec)
        self.tospl = int(sampling_freq*self.tosec)
        self.data = data[self.fromspl:self.tospl]

    def from_slice(self, fromsec, tosec):
        return Audio(self.data, self.sf, fromsec, tosec)

    @classmethod
    def read_sound(cls,
                   fn,
                   channel=CFG.audio_channel_n,
                   resample_freq=CFG.audio_resample_freq):
        """
        Return the sample audio data and the sampling frequency of a given
        sound file. Returns audiodata averaged over all channels, or
        a specific channel (0:left, 1:right) if that argument is given.
        Resamples to resample_freq, if given and >0
        """
        logging.info("reading audio...")
        (data, sampling_freq) = soundio.wavread(fn)
        logging.info("read audio ({} samples, dtype={}, sf={}, min={}, max={}, mean={}) from file '{}'".format(
                    len(data),        data.dtype, sampling_freq, data.min(), data.max(), data.mean(),      fn))
        if resample_freq and resample_freq != sampling_freq:
            logging.info("resampling audio...")
            fnres = soundio.resample(fn)
            (data, sampling_freq) = soundio.wavread(fnres)
            logging.info("resampled audio (now {} samples, dtype={}, sf={}, min={}, max={}, mean={})".format(
                len(data), data.dtype, sampling_freq, data.min(), data.max(), data.mean()))
        #if asked for specific channel, return that
        if 0 <= channel < len(data):
            data = data[channel]
        #else if not mono, then average the channels (>2 accepted, though unusual)
        elif data.ndim != 1:
            data = data.mean(axis=1)
        return cls(data, sampling_freq)

    def write(self, chunk_file):
        #fromframeno = int(self.fromsec * self.sf)
        #toframeno = int(self.tosec * self.sf)
        #d = self.data[fromframeno:toframeno]
        soundio.wavwrite(chunk_file, self.sf, self.data)

class Chunk(Audio):
    def __init__(self, *args, **kwargs):
        lpause_start = kwargs.pop("lpause_start", None)
        rpause_end = kwargs.pop("rpause_end", None)
        super(Chunk, self).__init__(*args, **kwargs)
        assert (lpause_start or float("-inf")) <= self.fromsec <= self.tosec <= (rpause_end or float("inf")), (
            str((lpause_start, self.fromsec, self.tosec, rpause_end, self)))
        self.lpause_start = lpause_start
        self.rpause_end = rpause_end


    def __str__(self):
        return "Chunk({}-{})".format(self.fromsec, self.tosec)

    def __repr__(self):
        return "Chunk({}-{})".format(self.fromsec, self.tosec)

    def lpause_dur(self):
        return (None if self.lpause_start is None else
                self.fromsec - self.lpause_start)

    def rpause_dur(self):
        return (None if self.rpause_end is None else
                self.rpause_end - self.fromsec)

    @classmethod
    def from_sound(cls, sound, fromsec, tosec, lpause_start, rpause_end):
        return cls(sound.data,
                   sound.sf,
                   fromsec=fromsec,
                   tosec=tosec,
                   lpause_start=lpause_start,
                   rpause_end=rpause_end)

def main():
    import chunker
    logging.basicConfig(level=logging.INFO)
    soundfile = sys.argv[1]
    outdir = "output/"
    audio1 = Audio.read_sound(soundfile)
    intensity = chunker.compute_intensity(audio1)
    for perc in range(60, 45, -1):
        (threshold, _times_rel, chunks) = audio_chunker.get_silences(audio1, perc)
        print("at percentile %d threshold = %0.7f: found %d pause candidates" % (
                             perc,          threshold,   len(chunks)))
        for (pause, sound) in chunks:
            print("%0.5f - %0.5f" % (pause, sound))
            write_audio_chunk(outdir, audio1, pause, sound)


if __name__ == '__main__':
    main()
