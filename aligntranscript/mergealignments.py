#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals


import cmd
from codecs import open
from collections import defaultdict

import numpy

from config import CFG
import soundio, linecol
from serialize import elan, html
from audio import Audio
from utils.clicolor import printok, printerr, printwarn, cprint
from utils.six import letters, input
from utils.time import secs2hmsstr

class NoArgumentError(Exception):
    pass

class ChunkIndexError(Exception):
    pass

class ForceRecomputationError(Exception):
    pass

class InteractiveAlignmentMerger(cmd.Cmd):
    chunk_labels = letters #a-zA-Z
    inspect_threshold = CFG.pause_inspect_threshold
    adjust_threshold = CFG.pause_adjust_threshold
    prompt = "(IA MODE): l[ist], p[lay], c[onfirm]; or h[elp] for more> "

### INTERFACE
#* do_l, do_list [LINES]                         (any number of lines, default all; no chunks allowed)
#* do_p, do_play, do_after, do_before LINECHUNKS (any number of lines; any number of chunks for each)
#* do_c, do_confirm LINECHUNK-PAIRS              (any number of lines; exactly one chunk for each)
#* do_unconfirm LINES                            (any number of lines; no chunks allowed)
#* do_realign LINE LINECHUNK-PAIR                (one line, one line-chunk pair)
#* do_recompute
#* do_write_elan, do_write_txt, do_write_html, do_write_praat [FILENAME]
#* do_blip [on|off]
#* do_adjust_threshold, do_inspect_threshold [THRESHOLD]
#* do_EOF





#### initialize
    def __init__(self, audio, transcript, alignments, **kwargs):
        cmd.Cmd.__init__(self, **kwargs)
        self.audio = audio
        self.transcript = transcript
        self.alignments = alignments
        self.transcript_lines = [None] + transcript.trans_lines()
        self.linecount = len(transcript.trans_lines())
        self.colcount = len(alignments)
        self.colwidth = max(self.colcount, 20)
        self._lastlist = None  #cache the parameters of last 'list' command

        self.audio_context = CFG.cli_play_context
        self.audio_blip = False
        self.audio_blip_hi = Audio.read_sound(CFG.cli_blip_hi_file, resample_freq=audio.sf)
        self.audio_blip_lo = Audio.read_sound(CFG.cli_blip_lo_file, resample_freq=audio.sf)
        chunk_cands = defaultdict(dict)

        bestmatch = dict()
        for (char, algms) in zip(self.chunk_labels, self.alignments):
            for (paraid, (para, algm)) in enumerate(zip(transcript, algms)):
                if algm is None:
                    continue
                firstline = para[0]
                firstchunk = algm[0]
                if firstline.start is not None and firstline.end and firstchunk.fromsec is not None and firstchunk.tosec:
                    dev = abs(firstline.start - firstchunk.fromsec) + abs(firstline.end - firstchunk.tosec)
                    if paraid not in bestmatch or (dev, char) < bestmatch[paraid]:
                        bestmatch[paraid] = (dev, char)
                for (lineid, chunk) in enumerate(algm, para.startline+1):
                    chunk_cands[lineid][char] = chunk
        self.chunk_cands = chunk_cands
        self.confirmed = dict()
        for (paraid, chunkiddev) in bestmatch.items():
            chunkid = chunkiddev[1]
            self.confirmed[transcript[paraid][0].lineid] = chunkid
        self.do_list("")

    do_h = cmd.Cmd.do_help
#    def do_h(self, _line):
#        cmd.Cmd.do_help(self, _line)


    def do_realign(self, line):
        """\
realign LINE1 LINE2-CAND
Used (only) when none of the suggested candidates for LINE1 is appropriate,
but one of the candidates of LINE2 indeed is. For instance, if line 8 can
indeed be heard by playing 11m, then you say

realign 8 11m

Realign will automatically recompute -- you should check that all confirmed
alignments are OK before using it
"""
        args = line.strip().split()
        if len(args) != 2:
            raise NoArgumentError("expected 2 arguments, found {}".format(len(args)))

        arg1 = self.linecol(args[0], required_cols="==0")
        arg2 = self.linecol(args[1], required_cols="==1")
        if not (1 == len(arg1.lines) and 0 == len(arg1.linecols)):
            raise ValueError("arg1: expected single line id")
        if not 1 == len(arg2.lines) == len(arg2.linecols):
            raise ValueError("arg2: expected line-chunk pair")
        realign = {arg1.lines[0]:arg2.linecols[0]}
        self.write_txt(CFG.text_output_file, realign=realign)
        raise ForceRecomputationError

    def do_recompute(self, _line):
        """\
recompute:
writes current alignments to file and recomputes
new alignments for the missing ones given this information.
Conceptually amounts to restarting the program with new input
(and may thus take a long time). Should seldom be necessary on its own.
(When none of the candidates for a line are appropriate, use realign).
"""
        self.write_txt(CFG.text_output_file)
        raise ForceRecomputationError

#### show lines

    def do_list(self, line):
        """\
list [LINES]:
list all lines specified by LINES, or same as last one if LINES is not given.
The special form list ALL will always do that
Example LINES are
8
4-6
3,11,5
1-3,5,2
"""
        self._filter_chunks()
        startlines = self.transcript.startlines()
        if line.strip() == "ALL":
            line = "{}-{}".format(1, self.linecount)
        elif not line.strip():
            line = self._lastlist or "{}-{}".format(1, self.linecount)
        linecols = self.linecol(line, required_cols="==0")
        self._lastlist = line
        all_chunkids = sorted(set([k for v in self.chunk_cands.values() for k in v.keys()]), key=swapcase)
        def header():
            print("ID".center(4, "-"),
                  "CHUNK".center(len(all_chunkids), "-"),
                  "TRANSCRIPTION".center(50, "-"))
        header()
        for lineid in linecols.lines:
            syncedbullet = ("+" if lineid in self.confirmed else
                           "-" if lineid in startlines else
                           " ")
            line = self.transcript_lines[lineid]
#            chunkids = sorted(self.chunk_cands[lineid])
            #1. print line number
            print("{}{:4d} ".format(syncedbullet, lineid), end="")
            #2a. if confirmed, print times
            if lineid in self.confirmed:
                chunkid = self.confirmed[lineid]
                ch = self.chunk_cands[lineid][chunkid]
                times = "{}:{:8.2f}-{:8.2f}".format(chunkid, ch.fromsec, ch.tosec)
                cprint(" " * (self.colwidth-len(times)) + times, color='green', end="")
            #2b. else, print candidates
            else:
                for c in all_chunkids:
                    if c not in self.chunk_cands[lineid]:
                        cprint("-", color='blue', end="")
                        continue
                    cd = self.chunk_cands[lineid][c].cand
                    if   cd == "inspect":
                        cprint(c, color='magenta', end="")
                    elif cd == "adjust":
                        cprint(c, color='yellow', end="")
                    elif cd == "hide":
                        cprint(" ", color='white', end="")
                    elif cd == "discard":
                        cprint(c, color='blue', end="")
                    else:
                        raise ValueError("unknown chunk candidate class")
                #3. print line content (must go last, no fixed width ipa font)
            cprint(" {}".format(line), color='cyan')
    do_l = do_list  #short form

    def do_threshold_adjust(self, line):
        """\
threshold_adjust [THR]:
Chunks which differ from a candidate by less than THR seconds will be suppressed
in listings. Without argument, shows current setting.
"""
        if line.strip():
            try:
                thr = float(line.strip())
            except:
                raise ValueError("expected threshold value (in seconds), got {}".format(line))
            self.adjust_threshold = thr
        printok("adjust threshold is {} seconds".format(self.adjust_threshold))

    def do_threshold_inspect(self, line):
        """\
threshold_inspect [THR]:
Chunks which differ from a recognized candidate by more than THR seconds will
be shown as main candidate in listings. Without argument, shows current setting.
"""
        if line.strip():
            try:
                thr = float(line.strip())
            except:
                raise ValueError("expected threshold value (in seconds), got {}".format(line))
            self.inspect_threshold = thr
        printok("inspect threshold is {} seconds".format(self.inspect_threshold))


#### play audio
    def do_blip(self, line):
        """\
blip on|off:
blip=on marks the beginning and end of a sound with a short beep.
Does not affect data in any way, only what is played back.
Without argument, shows current setting."""
        if line.strip():
            blip = True if line.strip().lower() in ["on", "true", "1"] else False
            self.audio_blip = blip
        printok("blip is now {}".format("on" if self.audio_blip else "off"))

    def _play_sound_time(self, fromsec, tosec, lbl):
        fromspl = int(self.audio.sf*fromsec)
        tospl = int(self.audio.sf*tosec)
        data = self.audio.data[fromspl:tospl]
        (absfile, _relfile) = html.audio_filename(data, fromsec, tosec, lbl)
        if self.audio_blip:
            data = numpy.concatenate([
              self.audio_blip_hi.data,
              data,
              self.audio_blip_lo.data])
        soundio.wavwrite(absfile, self.audio.sf, data)
        soundio.wavplay(absfile)


    def _play_sound_chunk(self, lineid, chunkid, playwhat="sound"):
        assert playwhat in {"sound", "lpause", "rpause"}
        line = self.transcript_lines[lineid]
        chunk = self.chunk_cands[lineid][chunkid]
        times = {"sound": (chunk.fromsec, chunk.tosec),
                 "lpause": (chunk.lpause_start, chunk.fromsec),
                 "rpause": (chunk.tosec, chunk.rpause_end)}
        label = {"sound":"sound", "lpause":"left pause", "rpause":"right pause"}
        fsec, tsec = times[playwhat]
        if fsec >= tsec:
            printerr("skipping empty sound ({:0.3f}--{:0.3f})".format(fsec, tsec))
            return
        printok("{}{}: playing {} [TIME:{:0.3f}-{:0.3f}] of line ".format(
                  lineid, chunkid,  label[playwhat], fsec, tsec), end=" ")
        cprint(line, color="cyan")
        self._play_sound_time(fsec, tsec, chunkid)


    def _play_sequence_chunk(self, linecols, playwhat):
        howmanyleft = len(linecols.linecols)
        total = howmanyleft
        for (lineid, chunkid) in linecols.linecols:
            howmanyleft -= 1
            if chunkid not in self.chunk_cands[lineid]:
                msg = 'no pairing for line {}, chunk {} (skipping)'.format(lineid, chunkid)
                printerr(msg)
                #raise ChunkIndexError(msg)
                continue
            self._play_sound_chunk(lineid, chunkid, playwhat)
            if howmanyleft > 0:
                q = input(" enter 'q' to leave queue, anything else to play next item ({}/{}): ".format(
                    1+total-howmanyleft, total))
                if q == "q":
                    break


    def do_play(self, line):
        """\
play LINECHUNKS:
plays the audio chunks identified by LINECHUNKS"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        linecols = self.linecol(line, required_cols=">=1")
        self._play_sequence_chunk(linecols, playwhat="sound")

    do_p = do_play #short form

    def do_play_confirmed(self, line):
        """
play_confirmed
plays all confirmed chunks in order
        """
        if not line.strip():
            line = self._lastlist or "{}-{}".format(1, self.linecount)
        linecols = self.linecol(line, required_cols="==0")
        selection = set(l for l in linecols.lines)
        playthese = " ".join(["{}{}".format(k, v) for k,v in sorted(self.confirmed.items()) if k in selection])
        self.do_play(playthese)

    def do_play_before(self, line):
        """\
play_before LINECHUNKS:
plays the pauses to the left of the chunks identified by LINECHUNKS"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        linecols = self.linecol(line, required_cols=">=1")
        self._play_sequence_chunk(linecols, playwhat="lpause")

    def do_play_after(self, line):
        """\
play_after LINECHUNKS:
plays the pauses to the right of the chunks identified by LINECHUNKS"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        linecols = self.linecol(line, required_cols=">=1")
        self._play_sequence_chunk(linecols, playwhat="rpause")

    def do_add_note(self, line):
        """\
add_note LINE NOTE 
prepends NOTE (e.g., a warning or a note to check up later) to LINE
NOTE may contain spaces.
For instance, 
add_note 27 !!CHECK THIS!!
Notes are not easy to remove again. You can only add them to one line at 
a time
"""
        linenote = line.strip().split(None, 1)
        if not len(linenote) == 2:
            note = "!!CHECKME "
        else:
            (line, note) = linenote
        lineid = int(line)
        self.transcript_lines[lineid] = note + self.transcript_lines[lineid]
        printwarn("prepended note {} to line {}".format(note, lineid))

#### edit line-chunk pairings

    def do_confirm(self, line):
        """\
confirm LINECHUNK-PAIRS
confirms that annotation lineid is a transcription of sound chunkid
Example LINECHUNK-PAIRS are
8a        #confirms candidate a for lineid 8 (i.e, candidate 8a)
2,4-6a    #confirms candidate a for lineid 2,4,5,6
          #  (i.e, candidate 2a for 2, 4a for 4, etc)
2a 4b     #confirms candidate a for lineid 2 and candidate b for line 4
2,4-6a-b  #illegal (only one candidate chunk per line is allowed)
"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        linecols = self.linecol(line, required_cols="==1")
        for (lineid, chunkid) in linecols.linecols:
            if chunkid not in self.chunk_cands[lineid]:
                printerr("skipping, chunk {} is not a candidate for line {} ".format(chunkid, lineid))
                continue
            elif lineid in self.confirmed:
                printerr("skipping, already confirmed: {} (unconfirm first if you really wanted the change)".format(lineid))
                continue
            else:
                self.confirmed[lineid] = chunkid
                printok("confirmed: line {} paired with chunk {}".format(lineid, chunkid))

    do_c = do_confirm #short form

    def do_unconfirm(self, line):
        """\
unconfirm LINES
unconfirms (i.e., removes confirmed candidate chunk) for all lines in LINES.
Also removes any other inferences for preceding and following lines (e.g,
confirming that a line starts at 8.50 means that preceding lines cannot end
later than that).
"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        linecols = self.linecol(line, required_cols="==0")
        for lineid in linecols.lines:
            if lineid in self.confirmed:
                del self.confirmed[lineid]
                printok("unconfirmed: line {} no longer paired with any chunk ".format(
                                         lineid))





#### write output
    def do_write(self, line):
        """\
write FORMAT [OUTPATH]
writes current state of annotations to file in FORMAT, where FORMAT is one of
  html  -- (*2,4)
  txt   -- plain transcription text enhanced with timestamps (*1,5)
  elan  -- eaf annotation file for elan (xml) (*3,5)
  praat -- praat TextGrid (*3,5)  #NOT YET IMPLEMENTED

Notes:
(*1) For html, non-disambiguated annotations will be marked as such and
written in a table, for ease of access
(*2) For txt, non-disambiguated annotations will simply be written without
timestamps.
(*3) For elan and praat, all annotations must (currently) be disambiguated,
i.e. confirmed to exactly one time interval.
(*4) OUTPATH is a directory (which must exist) into which all associated
files are written; the entry point is OUTPATH/index.html
(*5) If not given, OUTPATH is original sound but with file extension replaced by
'txt', 'eaf', 'TextGrid' as appropriate

Avoid whitespace in OUTPATH, it is likely to cause problems.

"""
        if not line.strip():
            raise NoArgumentError("expected >=1 arguments, found 0")
        args = line.split(None, 1)
        fmt = args.pop(0)
        goodformats = {
              'html': self.write_html,
              'txt': self.write_txt,
              'elan': self.write_eaf,
              'praat': self.write_praat}
        if fmt not in goodformats:
            printerr("format '{}' not in allowed [{}] (ignoring)".format(
                           fmt, ", ".join(list(goodformats.keys()))))
            return
        outpath = args[0] if args else ""
        goodformats[fmt](outpath)

    def write_html(self, outpath):
        htmldir = outpath if outpath.strip() else CFG.html_output_dir
        sound = self.audio
        self._filter_chunks()
        def cf(lineid, chunkid, cand):
            if self.confirmed.get(lineid, None) == chunkid:
                return "confirmed"
            else:
                return cand
        labels = [(l, a[0].cost, a[0].chunking.perc, a[0].chunking.thr)
                    for (l, a) in zip(self.chunk_labels, self.alignments)]
        annots = []
        for (lineid, line) in enumerate(self.transcript_lines[1:], 1):
            labelchunks = sorted(self.chunk_cands[lineid].items(), key=lambda xy: (swapcase(xy[0]),xy[1]))
            chunks = [(chunkid, c.fromsec, c.tosec, cf(lineid, chunkid, c.cand))
                      for (chunkid, c) in labelchunks]
            annots.append( (lineid, line, chunks) )
        html.write_html(annots, labels, sound, htmldir)
        #annots :: [(lineid, line, [(label, fromsec, tosec, cand)])]

    def write_txt(self, outpath, realign=None):
        if realign is None:
            realign = dict()
        txtfile = outpath if outpath.strip() else CFG.text_output_file

        with open(txtfile, "w", encoding="utf-8") as f:
            for (lineid, line) in enumerate(self.transcript_lines[1:], 1):
                hint = self.transcript.hints.get(lineid-1, None)
                if hint:
                    print("<{0:0.2f}>".format(hint), file=f)
                if lineid not in realign and lineid not in self.confirmed:
                    times = ""
                else:
                    if lineid in realign:
                        lineid0, chunkid0 = realign[lineid]
                        ch = self.chunk_cands[lineid0][chunkid0]
                    else:
                        chunkid = self.confirmed[lineid]
                        ch = self.chunk_cands[lineid][chunkid]
                    ftime = secs2hmsstr(ch.fromsec, precision=2)
                    ttime = secs2hmsstr(ch.tosec, precision=2)
                    times = "{0:s}--{1:s}".format(ftime, ttime)
                    #times = "{0:s}".format(ftime)
                print("{{{}}} {}".format(times, line), file=f, end=CFG.transcript_spliton)

        printok("wrote {} chunks ({} confirmed) to file {}".format(
            self.linecount, len(self.confirmed), txtfile))

    def write_eaf(self, outpath):
        cf =  len(self.confirmed)
        if cf != self.linecount:
            printerr("expected {} confirmed lines, found {} (ignoring)".format(self.linecount, cf))
            return
        eaffile = outpath if outpath.strip() else CFG.elan_output_file
        sound = self.audio
        annots = []
        for (lineid, line) in enumerate(self.transcript_lines[1:], 1):
            chunkid = self.confirmed[lineid]
            c = self.chunk_cands[lineid][chunkid]
            annots.append( (line, c.fromsec, c.tosec) )
        elan.write_elan(annots, sound, eaffile)


    def write_praat(self, outpath):
        praatfile = outpath if outpath.strip() else CFG.praat_output_file
        raise NotImplementedError("praat output to {}: TBD".format(praatfile))


#### general helper
    def linecol(self, line, required_cols, nonempty_req=True):
        return linecol.LineCol.from_string(line, required_cols=required_cols,
            labelcount=self.colcount, lolimit=1, hilimit=self.linecount)

#### user interface
    def emptyline(self):
        return

    def do_EOF(self, _line):
        raise KeyboardInterrupt

    def do_quit(self, _line):
        """type quit (or Ctrl+D) to quit"""
        raise KeyboardInterrupt

    def _bisect(self):
        import bisect
        cfd = []
        for (lineid, chunkid) in sorted(self.confirmed.items()):
            c = self.chunk_cands[lineid][chunkid]
            cfd.append((lineid, chunkid, c))

        def conf_limits(lineid):
            #if lineid in self.confirmed:
            #    c = self.chunk_cands[lineid][self.confirmed[lineid]]
            #    return (c.fromsec, c.tosec)
            i = bisect.bisect_left(cfd, (lineid, "0", None))
            leftlim = cfd[i-1][2].tosec if 0 <= i-1 < len(cfd) else None
            rightlim = cfd[i][2].fromsec if 0 <= i < len(cfd) else None
            return (leftlim, rightlim)
        return conf_limits


    def _filter_chunks(self):
        filter_confirmed = self._bisect()
        for lineid in range(1, self.linecount+1):
            currstart = self.audio.fromsec/2.0
            currend = currstart
            (leftlim, rightlim) = filter_confirmed(lineid)
            for (chunkid, ch) in sorted(self.chunk_cands[lineid].items()):
                if lineid in self.confirmed:
                    if self.confirmed[lineid] == chunkid:
                        ch.cand = "confirmed"
                    else:
                        ch.cand = "hide"
                    continue
                deltastart = ch.fromsec - currstart #normally positive
                deltaend = currend - ch.tosec       #normally positive
                dt = abs(deltastart) + abs(deltaend)
                if deltastart < 0.0 or deltaend < 0.0:
                    ch.cand = "inspect"
                elif abs(dt) > self.inspect_threshold:
                    ch.cand = "inspect"
                elif abs(dt) > self.adjust_threshold:
                    ch.cand = "adjust"
                else:
                    ch.cand = "hide"
                if abs(dt) > self.adjust_threshold or deltastart < 0.0 or deltaend < 0.0:
                    (currstart, currend) = (ch.fromsec, ch.tosec)

                if (ch.cand != "hide" and leftlim and ch.fromsec < leftlim):
                    ch.cand = "discard"
                elif (ch.cand != "hide" and rightlim and ch.tosec > rightlim):
                    ch.cand = "discard"

def swapcase(c):
    if c.isupper(): return c.lower()
    elif c.islower(): return c.upper()
    else: return c
