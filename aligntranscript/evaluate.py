#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals


import logging, math


def gold_std(segno):
    xs = [
        (1.499098571099821  ,  2.9526249625441636 ),
        (3.564583881915235  ,  4.576196754479709  ),
        (5.313410131354913  ,  6.2333028132139265 ),
        (7.119789384807005  ,  8.009005530161192  ),
        (8.955084010893998  ,  9.870900639228864  ),
        (11.092467313451776 , 12.50947830829921   ),
        (13.283033319465279 , 14.301451810725629  ),
        (15.110866507137775 , 15.977282238508522  ),
        (16.823038560622148 , 17.86425746586594   ),
        (18.711672852250487 , 19.616750220229346  ),
        (20.631368642175694 , 21.44838347658232   ),
        (22.371800242911664 , 23.3781213363533    ),
        (24.22767772521353  , 25.200495388506997  ),
        (25.942208224048194 , 27.044228231388917  )]
    return xs[segno]

def evaluate(annots, orig_lines):
    cost = 0
    for (trans_ind, (start, end, trans)) in enumerate(annots):
        (start_gs, end_gs) = gold_std(trans_ind)
        startc = abs(math.log(start/start_gs)) if start > 0 and start_gs > 0 else 1000
        endc = abs(math.log(end/end_gs)) if end > 0 and end_gs > 0 else 1000
        logging.info("[%7.3f - %7.3f] [%7.3f - %7.3f]", start, end,   startc, endc)
        logging.info("%s %s", " ".join(trans), orig_lines[trans_ind])
        cost = cost + startc + endc
    logging.info("COST wrt gold standard: %s", cost)
