#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import itertools, re

from utils.six import letters

class LineCol(object):
    def __init__(self, lines, linecols):
        self.linecols = linecols
        self.lines = lines

    def __repr__(self):
        return "LineCol({!r}, {!r})".format(self.lines, self.linecols)

    @classmethod
    def from_string(cls, line, **kwargs):
        return cls.from_strings(line.split(), **kwargs)

    @classmethod
    def from_strings(cls, args, **kwargs):
        lines, linecols = [], []
        for arg in args:
            (l, c) = parse(arg, **kwargs)
            linecols.extend(itertools.product(l, c))
            lines.extend(l)
        return cls(lines, linecols)

class IntervalParseError(Exception):
    pass

def parse(s, required_cols=None, labelcount=None, lolimit=None, hilimit=None):
#required_cols (per given line): "==0", "==1", ">=1", ">=0", None (same as ">=0")
    permitted_labels = set(letters[:labelcount])
    def parse_part(s, atom, computerange):
        result = []
        if not s.split():
            return []
        for iv in s.split(","):
            if re.match(r'^({})$'.format(atom), iv.strip()):
                result.append(iv)
            else:
                m = re.match(r'^({})-({})$'.format(atom, atom), iv.strip())
                if m:
                    (start, end) = m.groups()
                    r = computerange(start, end)
                    result.extend(r)
                else:
                    raise IntervalParseError("unparsable interval spec: {}".format(iv))
        return result
    def computerangechar(start, end):
        s = letters.find(start)
        e = letters.find(end)
        return letters[s:e+1] if s >= 0 and e >= 0 else []
    def computerangeint(start, end):
        s = int(start) if lolimit is None else max(int(start), lolimit)
        e = int(end) if hilimit is None else min(int(end), hilimit)
        return [str(i) for i in range(s, e+1)]

    m = re.search(r'(.*\d)($|[a-zA-Z].*)', s)
    if m is None:
        raise IntervalParseError(
            "unparsable interval spec: " + s)
    else:
        (lines, cols) = m.groups()
        lineatom = r'\d+'
        colatom = r'[a-zA-Z]'
        ls = [int(i) for i in parse_part(lines, lineatom, computerangeint)
             if lolimit <= int(i) <= hilimit]
        if not ls:
            raise IntervalParseError(
                "no lines in permitted interval {}<=x<={}: {}".format(
                                            lolimit, hilimit, s))
        cs = []
        for c in parse_part(cols, colatom, computerangechar):
            if c not in permitted_labels:
                raise IntervalParseError("chunk label outside permitted set: " + c)
            else:
                cs.append(c)
        if required_cols is not None and not validate_chunk_labels(required_cols, cs):
            raise IntervalParseError(
                "expected {} column(s), found {}: {}".format(required_cols, len(cs), cs))
        return (ls, cs)

def validate_chunk_labels(required_cols, cs):
    if required_cols is None: return True
    elif required_cols == ">=0": return True
    elif required_cols == ">=1": return len(cs) >= 1
    elif required_cols == "==1": return len(cs) == 1
    elif required_cols == "==0": return len(cs) == 0
    else: raise ValueError("unknown constraint: " + required_cols)

if __name__ == '__main__':
    import sys
    a = LineCol.from_strings(sys.argv[1:])
    print("lines", a.lines)
    print(a)
    for kv in a.linecols:
        print(kv)
