#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
module transcript contains classes which deals with transcriptions
of speech recordings. Generally, each record (Line) is assumed to
correspond to a single utterance in the audio -- i.e., . However, in order
to integrate partial time information (e.g, "this line begins at
8:42.13 and ends at 8.43.27. in the audio source", or "the pause
preceding this line contains the point 7.32.00" (very useful)), a sequence of Line()s with
known start time is grouped into a Para(). A Transcript is a
sequence of Para()s.
"""

from __future__ import division, print_function, unicode_literals

from codecs import open
import logging, re, sys

from utils.time import hms2secs
from config import CFG


TIME_RXS = {
    "{}": re.compile(r'\{([^}]*)\}\s*'),
    "<>": re.compile(r'<([^>]*)>\s*')}


class Line():
    def __init__(self, lineid, trans_line, start=None, end=None, orig_line=""):
        self.lineid = lineid
        self.trans_line = trans_line
        self.start = start
        self.end = end
        self.orig_line = orig_line or trans_line
        self.rel_len = None

    def __str__(self):
        return "{}: ({}--{}) {}".format(
            self.lineid,
            None if self.start is None else round(self.start, 2),
            None if self.end is None else round(self.end, 2),
            self.trans_line)

    def __repr__(self):
        return "Line({},start={},end={},trans_line='{}')".format(
            self.lineid, self.start, self.end, self.trans_line)

class Para():
    def __init__(self, lines, start, end, start_line, hint=None, lengths_by=CFG.transcript_lengths_by):
        self.lines = lines
        self.start = start
        self.end = end
        self.startline = start_line
        self.hint = hint
        self.statistics = self._compute_stats()
        relative_lengths = self._compute_relative_lengths()[lengths_by]
        assert len(lines) == len(relative_lengths)
        for (rel_len, line) in zip(relative_lengths, self.lines):
            line.rel_len = rel_len

    def relative_lengths(self):
        return [line.rel_len for line in self.lines]

    def __getitem__(self, i):
        if not -len(self.lines) <= i < len(self.lines):
            raise IndexError("index out of range")
        return self.lines[i] if i >= 0 else self.lines[len(self.lines)+i]

    def __str__(self):
        return "\n".join(str(l) for l in self.lines)

    def __repr__(self):
        return "Para(start={},end={}, startline={}, hint={}, lines=[\n  {}])".format(
            self.start, self.end, self.startline, self.hint, ",\n  ".join(repr(l) for l in self.lines))

    def __len__(self):
        return len(self.lines)

    def words(self):
        return [[w for w in t.split()] for t in self.trans_lines()]

    def trans_lines(self):
        return [line.trans_line for line in self.lines]

    def _compute_stats(self):
        return compute_stats(self.trans_lines(), self.words())

    def _compute_relative_lengths(self):
        return compute_relative_lengths(self.words(), self.statistics)

    def is_finished(self):
        return all(l.start is not None and l.end is not None for l in self.lines)

class Transcript():
    """
    Represents a transcription file
    """

    def __init__(self, paras, spliton=CFG.transcript_spliton,
                              filename=CFG.transcript_file):
        self.paras = paras
        self.spliton = spliton
        self.filename = filename
        self.statistics = self._compute_stats()
        self.hints = {p.startline: p.hint for p in self.paras}

    @classmethod
    def from_string(cls, transcript, filename, spliton=CFG.transcript_spliton, margin=0.25):
        transcript = transcript.strip()
        transcript = re.sub(r'\[[^]]*?\]+', r'', transcript)
        orig_lines = [l.strip() for l in transcript.split(spliton) if l.strip() and not l.startswith("#")]
        timed_lines = [find_times(l) for l in orig_lines]
        time_hints = [find_hints(l) for l in orig_lines]
        #orig_lines = [re.sub(r'\{[^}]*?\}+', r'', l) for l in orig_lines]
        orig_lines = [TIME_RXS["{}"].sub(r'', l) for l in orig_lines]
        orig_lines = [TIME_RXS["<>"].sub(r'', l) for l in orig_lines]
        orig_lines = [l.replace("\n", " ") for l in orig_lines]
        trans_lines = [strip_punctuation(l.lower().strip()) for l in orig_lines]
        paras, parastarts, hints = [], [], []
        for (lineid, (orig_line, trans_line, (start, end), starthint)) in \
          enumerate(zip(orig_lines, trans_lines, timed_lines, time_hints), 1):
            if lineid == 1 or start or starthint:
                prevend = timed_lines[lineid-1-1][1]
                #note: start may be None
                prevmid = (prevend + start)/2 if prevend and start else max(start - margin, 0.0) if start else None
                parastart = 0.0 if lineid == 1 else starthint if starthint else prevmid
                paras.append([])
                parastarts.append(parastart)
                hints.append(starthint)
            paras[-1].append(Line(lineid, trans_line, start, end, orig_line))
        startends = zip(parastarts, parastarts[1:] + [None], hints)
        times = []
        startline = 0
        starttime = 0.0
        for i in range(len(paras)):
            start = paras[i][0].start
            nextstart = paras[i+1][0].start if i+1 < len(paras) else None
            endtime = paras[i][-1].end or nextstart
            times.append((starttime, endtime, startline))
            startline += len(paras[i])
            starttime = nextstart
        assert 0 < len(times) == len(paras) == len(parastarts), "{}!={}".format(len(times), len(paras))
        ps = [Para(lines, pstart, pend, stl, hint) for (lines, (pstart, pend, hint), (stt, endt, stl))
               in zip(paras, startends, times)]
        return cls(ps, filename=filename, spliton=spliton)


    def __getitem__(self, i):
#        if not 0 <= i < len(self.paras):
#            raise IndexError("index out of range")
        return self.paras[i]

    def __setitem__(self, i):
        raise AttributeError("can't set item")

    def __delitem__(self, i):
        raise AttributeError("can't delete item")

    def __len__(self):
        return len(self.paras)

    def __str__(self):
        s = [["{}".format(l) for l in p] for p in self.paras]
        return "\n\n".join("\n".join(x) for x in s)

    def __repr__(self):
        return "Transcript(filename={}, paras=[\n{}])".format(
            self.filename, ",\n".join(repr(p) for p in self.paras))

    def words(self):
        return [[w for w in line.split()] for line in self.trans_lines()]

    def trans_lines(self):
        return [line.trans_line for line in self.lines()]

    def lines(self):
        return [line for para in self.paras for line in para]

    @classmethod
    def read_from_file(cls, filename, **kwargs):
        logging.info("reading transcript...")
        with open(filename, "r", encoding="utf-8") as f:
            t = cls.read_from(f, filename, **kwargs)
        logging.info("read transcript ({} lines, {} words, {} chars) "
                     "from file '{}'".format(
            t.statistics['linecount'],
            t.statistics['wordcount'],
            t.statistics['charcount'],
            filename))
        return t

    @classmethod
    def read_from(cls, f, filename, **kwargs):
        return cls.from_string(f.read(), filename, **kwargs)

    def _compute_stats(self):
        return compute_stats(self.trans_lines(), self.words())

    def _compute_relative_lengths(self):
        return compute_relative_lengths(self.words(), self.statistics)

    def startlines(self):
        return set(p.startline+1 for p in self.paras)

def strip_punctuation(s):
    return re.sub(r'[–…,;:!?."\']', r'', s)

def find_hints(line):
    rx = TIME_RXS["<>"]
    m = rx.search(line)
    if m:
        times = m.group(1).strip()
        return hms2secs(times)

def find_times(line):
    returnnotime = (None, None)
    rx = TIME_RXS["{}"]
    m = rx.search(line)
    if not m:
        res = returnnotime
    else:
        times = m.group(1).strip()
        # noinspection PyTypeChecker
        c = times.count("--")
        if c == 0:
            res = (hms2secs(times), None)
        elif c == 1:
            (before, after) = re.split(r'\s*--\s*', times)
            (before, after) = (hms2secs(before), hms2secs(after))
            if before and after and before > after:
                raise ValueError("end time < start time? {}--{}".format(before, after))
            res = (before, after) if before or after else returnnotime
        else:
            res = returnnotime
    return res

def compute_stats(lines, words):
    stats = dict()
    stats['linecount'] = len(lines)
    stats['wordcount'] = sum(len(ws) for ws in words)
    stats['charcount'] = sum((sum(len(w) for w in ws) for ws in words))
    stats['syllcount'] = None #not implemented
    if stats['linecount'] <= 0 or stats['wordcount'] <= 0 or stats['charcount'] <= 0:
        logging.warning("empty stats? {} (lines: {} words {})".format(stats, lines, words))
    return stats

def compute_relative_lengths(words, statistics):
    (wc, cc) = (statistics['wordcount'], statistics['charcount'])
    assert wc > 0 and cc > 0
    lens = dict()
    lens["segment"] = [sum(len(w) for w in line)/cc for line in words]
    lens["word"] = [len(line)/wc for line in words]
    return lens


###############################################################################

def main():
    infile = sys.argv[1]
    with open(infile, encoding="utf-8") as f:
        t = Transcript.read_from(f, infile)
    print(t)
    print(repr(t))

    for (pid, para) in enumerate(t):
        for (lid, line) in enumerate(para):
            print("{:2d} {:2d} {:2d} {:7.2f} {:7.2f} {:.3f} {}".format(
    		    pid, lid, line.lineid, line.start or -1, line.end or -1, line.rel_len, line.trans_line))


if __name__ == '__main__':
    main()



