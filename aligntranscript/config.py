#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import argparse, logging, os.path, sys


CFG = argparse.Namespace(

    #transcript
    transcript_spliton="\n\n",
    transcript_lengths_by="segment", #word, segment [syllable TODO]
    transcript_punct_split_re=None,

    #audio
    audio_resample_freq=16000,
    audio_resample_bitdepth=16,
    audio_channel='average', #left, right, average

    #pauses
    pause_intensity_min_pitch=90,
    pause_perc_hi=70,
    pause_perc_lo=45,
    pause_perc_step=1,
    pause_min_ps_duration=0.2,
    pause_min_sd_duration=0.0,
    pause_inspect_threshold=0.5, #candidate contained in previous
                                 #cand but differing more than this
                                 #will be flagged for manual inspection
                                 #(un-contained ones always will)
    pause_adjust_threshold=0.08, #candidate differing less than this
                                 #from a previous cand will be hidden
                                 #in listings and html view

    #cli
    cli_blip_hi_file="RESOURCES/blip_hi.wav",
    cli_blip_lo_file="RESOURCES/blip_lo.wav",
    cli_play_context=1.0,

    #plots
    plot_chunks=False,
    plot_chunks_file="rel_utt_len.pdf",

    #evaluation
    evaluate=False, #TODO

    #output paths
    html_output_dir="OUTPUT", #html dir must exist
    html_output_view="list",
    #elan
    elan_validate=True,
    elan_schema_file="RESOURCES/EAFv2.7.xsd",

    #log
    log_level="info",
    log_file=sys.stdout,
    log_tofile_format="%(asctime)s: %(levelname)-8s %(message)s",
    log_toterm_format="%(levelname)-8s %(message)s",
    #%(name)s = "root" for simple cases

    #misc
    version=0.1,
    sox_path="/usr/bin/sox",

    #alignment is cubic in number of audio chunks -- suggest further
    # splitting when above this number
    warn_chunks = 100,

    #non-user-definable attributes (filled in from cl or computed later):
    progname=None,
    transcript_file=None,

    audio_file=None,
    audio_channel_n=None,

    elan_validator=None,
    log_level_n=None,
    log_file_format=None,

    elan_output_file=None,
    text_output_file=None,
    praat_output_file=None,

    pyaudio=None

)


def expand_nl_tab(s):
    return s.replace("<NL>", "\n").replace("<TAB>", "\t")


def setup(argv=None):
    if argv is None:
        argv = sys.argv

    parser = argparse.ArgumentParser(description=
    'Heuristically align pause-segmented transcripts with the audio source')

    parser.add_argument(dest='progname')                #!! ??
    parser.add_argument(dest='audio_file')
    parser.add_argument(dest='transcript_file')

    #### reading transcript
    parser.add_argument('--split-on', metavar="SEP",
                        dest='transcript_spliton', default=CFG.transcript_spliton,
                        help='split transcript file on (=pauses are marked by) SEP '
                             '(to deal with difficult escaping in shell, newline, tab '
                             'can be written <NL>, <TAB>) (def=<NL><NL>)')

    parser.add_argument('--transcript-lengths-by',
                        choices={'word', 'syllable', 'segment'},
                        dest="transcript_lengths_by", default=CFG.transcript_lengths_by,
                        help='estimate relative utterance length by UNIT (def=%(default)s)')

    #### reading audio
    parser.add_argument('--resample-freq', metavar='FREQ', type=int,
                        dest="audio_resample_freq", default=CFG.audio_resample_freq,
                        help='resample sound to FREQ (0 for no resampling) (deft=%(default)ss)')


    parser.add_argument('--channel',
                        choices={"left", "right", "average"},
                        dest="audio_channel", default=CFG.audio_channel,
                        help='read channel: left, right, or average (i.e., average '
                             'of all channels, normally 2 but possibly more) (default: %(default)s)')


    #### computing pauses
    parser.add_argument('--perc-hi', metavar='PERC', type=int,
                        dest='pause_perc_hi', default=CFG.pause_perc_hi,
                        help='upper bound for intensity percentile threshold (def=%(default)s)')

    parser.add_argument('--perc-lo', metavar='PERC', type=int,
                        dest='pause_perc_lo', default=CFG.pause_perc_lo,
                        help='lower bound for intensity percentile threshold (def=%(default)s)')

    parser.add_argument('--perc-step', metavar='STEP', type=int,
                        dest='pause_perc_step', default=CFG.pause_perc_step,
                        help='step percentile by STEP (def=%(default)s)')

    parser.add_argument('--min-pause', metavar='SEC', type=float,
                        dest='pause_min_ps_duration', default=CFG.pause_min_ps_duration,
                        help='minimum duration for pause (def=%(default)ss)')


    #### output options
    parser.add_argument('--evaluate', action="store_true", default=CFG.evaluate,
                        help='evaluate each suggestion against gold standard (def=%(default)s)')

    parser.add_argument('--html-output-dir', metavar='DIR',
                        default=CFG.html_output_dir,
                        help='html output goes to directory DIR (def=%(default)s)')

    parser.add_argument('--html-output-view',
                        choices={'list', 'table'},
                        default=CFG.html_output_view,
                        help='html view has form of table or (inlined) list (def=%(default)s)')

    parser.add_argument('--elan-validate', action="store_true", default=CFG.elan_validate,
                        help='validate elan xml against schema (NOTE: requires (a) lxml and '
                             '(b) an *.xsd schema for elan pointed to in config.CFG) (def= %(default)s)')


    #### log
    parser.add_argument('--log-file', dest="log_file",
                        type=argparse.FileType('w', 1),
                        default=CFG.log_file, help='logfile (def=stdout)')

    parser.add_argument('--log-level',
                        choices={'debug', 'info', 'warning'}, default=CFG.log_level,
                        help='logging level (def= %(default)s)')

    #### misc
    parser.add_argument('--version', action='version', version=CFG.version)

    cfg = parser.parse_args(argv, namespace=CFG)
    cfg.log_level_n = getattr(logging, cfg.log_level.upper())
    if cfg.log_file == sys.stdout:
        cfg.log_file_format = cfg.log_toterm_format
    else:
        cfg.log_file_format = cfg.log_tofile_format

    ### start logging
    logging.basicConfig(
        stream=CFG.log_file,
        level=CFG.log_level_n,
        format=CFG.log_file_format)

    #### preprocess
    if not os.path.exists(CFG.html_output_dir):
        logging.critical("output dir '{}' does not exist".format(CFG.html_output_dir))
        raise SystemExit()
    if not os.path.exists(CFG.sox_path):
        logging.critical("did not find sox at '{}' -- installed? ({})".format(
            CFG.sox_path, "http://sox.sourceforge.net/"))
        raise SystemExit()

    cfg.transcript_spliton = expand_nl_tab(cfg.transcript_spliton)
    cfg.audio_channel_n = {"left": 0, "right": 1, "average": -1}[cfg.audio_channel]


    (audiopath, _ext) = os.path.splitext(cfg.audio_file)
    cfg.elan_output_file = audiopath + ".eaf"
    cfg.text_output_file = audiopath + ".timealign.txt"
    if cfg.transcript_file != cfg.text_output_file and os.path.exists(cfg.text_output_file):
        logging.warning("reusing existing transcription file at {}, ignoring {}".format(
            cfg.text_output_file, cfg.transcript_file))
        logging.warning("if this was unintended, remove {} and restart".format(cfg.text_output_file))
        cfg.transcript_file = cfg.text_output_file
    cfg.praat_output_file = audiopath + ".TextGrid"
    try:
        import pyaudio
        cfg.pyaudio = pyaudio.PyAudio()
    except ImportError:
        logging.warning("pyaudio module not found -- you will not have "
            "playback from commandline (but you can still use the html view)")

CFG.setup = setup

if __name__ == '__main__':
    CFG.setup()
    from pprint import pprint

    pprint(vars(CFG))
