#!/usr/bin/env python3
import argparse
import numpy as np
import matplotlib.pyplot as plt
import sys
import audio


def prepare_data(data, normalize=False):
    length = max(len(d) for (label, d) in data)
    labels, res = [], []
    for (label, d) in data:
        s = sum(d) if normalize else 1
        tail = [0.0] * (length - len(d))
        res.append(np.array([x/s for x in (list(d)+tail)]))
        labels.append(label)
    res = np.array(res)
    return labels, res


#chunking plot does what seqplot does, but takes a sequence of Chunks as input
#chunks0 [Chunk(0.0-0.0098125), Chunk(0.27825-2.2476875),
# Chunk(3.3663125-6.512), Chunk(7.851875-8.92), Chunk(9.1304375-9.49975), Chunk(10.396625-11.2630625), Chunk(11.6545-13.44225), Chunk(13.8500625-15.3183125), Chunk(17.572125-17.6114375), Chunk(18.528875-19.042125), Chunk(19.843875-24.5245625), Chunk(24.8348125-24.896125),
# Chunk(25.4833125-26.1350625), Chunk(26.6240625-28.2344375), Chunk(28.9645625-28.9645625)]
#

def chunkingplot(labelled_chunkings, *args, **kwargs):
    labelled_data = []
    for (label, chunking) in labelled_chunkings:
        durations = []
        dummychunk = argparse.Namespace(tosec=0.0, fromsec=0.0)
        for (chunkl, chunkr) in zip([dummychunk] + chunking, chunking):
            durations.append(chunkl.tosec - chunkl.fromsec)
            durations.append(chunkr.fromsec - chunkl.tosec)
        labelled_data.append((label, durations))
    seqplot(labelled_data, *args, **kwargs)


def seqplot( perc_data,
             output_file,
             refdata=None,
             title="dummy title",
             normalize=True,
             width=0.7,
             perc_colors = ('g', 'lightgray'),
             ):
    """
    labelled data: sequence of tuples [(label, [x1, x2, ...])]
    output_file: name of file (duh)
    width: relative width of the bars (1.0 means no space between them)
    normalize: divide each value by the sum of the series it belongs to
    colors: use this sequence of colors (and repeat as necessary)

    """
    plt.figure()
    plt.suptitle(title)
    import matplotlib.gridspec as gridspec


    gs = gridspec.GridSpec(2, 1,height_ratios=[1,2.8])

    for (color, steps, thedata, subplotid, linewidth, aspect, gridid, alpha, ylabel) in [
          ('b', 30, refdata, 211, 0, 0.1, 0, "gradient", ""),
          (None, 1, perc_data, 212, None, None, 1, 0.3, "intensity threshold, percentile")]:
        if not thedata:
            continue
        ax = plt.subplot(gs[gridid])
        if aspect:
            ax.set_aspect(aspect)
        ax.set_xlim([0.0, 1.0])
        ax.set_ylabel(ylabel)
        (labels, data) = prepare_data(thedata, normalize=normalize)
        ind = np.arange(len(labels))    # the x locations for the groups

        #finding the left edges for each rectangle:
        #cumsum of same dimension as data, but starting on (0.0, 0.0, ...)
        #(and zip implicitly ignoring the ending on (total1, total2, ...) )
        pad = np.zeros((len(labels), 1))
        summed = np.cumsum(data, axis=1)
        leftedges = np.hstack([pad, summed])
        for colorind in range(len(data[0])):
            d = data[:,colorind]
            left = leftedges[:,colorind]
            color0 = perc_colors[colorind % len(perc_colors)] if color is None else color
            for i in range(steps):
                alpha0 = 1-i/steps if alpha == "gradient" else alpha
                d0 = d*(1/steps)
                left0 = left + d*(i/steps)
                plt.barh(bottom=ind, height=width, width=d0, color=color0, left=left0, alpha=alpha0, linewidth=linewidth)

        plt.yticks(ind+width/2., labels, fontsize=9)
        #plt.xticks(np.linspace(0, 1, 11))


    plt.savefig(output_file)

if __name__ == '__main__':
    kmwords = [int(k) for k in "4 9 6 10 8 9 5 4 7 8 13 3".split()]
    enwords = [int(k) for k in "5 5 3 5 5 4 6 6 5 12 3 3 5 4 6".split()]
    segs = [int(k) for k in "23 28 10 3 6 16 17 5 28 12 9 8 31".split()]
    words = [int(k) for k in "8 11 3 1 2 7 7 1 11 3 5 4 15".split()]
    refdata = [("segments", segs), ("words", words)]
    refdata2 = [("enwords", enwords), ("kmwords", kmwords)]
    labelled_data = []
#      ("x1", np.array([10, 20, 30, 40])),
#      ("x2", np.array([10, 25, 25, 40]))]

    seqplot(labelled_data, "rel_line_pos0.pdf", refdata2, title="Estimated relative position on line")
