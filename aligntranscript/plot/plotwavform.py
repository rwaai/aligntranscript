#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import math, os.path, sys
import pylab
import matplotlib.pyplot as plt
import matplotlib.collections as collections
import numpy as np
#import wave
import scipy.io.wavfile as siw
__author__ = 'ling-mun'

def plot_wave_spec(speechfile, outfile, annotdata):

    #read and prepare data
    with open(speechfile, "rb") as f:
        (framerate, data) = siw.read(f)
    stat_array(data, "original data")
    data = (data - data.mean()) / max(data.max(), -data.min())
    N = len(data)
    if len(data.shape) > 1:
        data = np.average(data, axis=1)
    tmax = N/framerate
    inds = np.linspace(0.0, tmax, N)
    print("read {} wave samples into {}-shaped array; framerate={}, tmax={}".format(
                  N, data.shape, framerate, tmax))
    sound_mask = np.zeros(N, dtype=np.bool)
    for (from_t, to_t, is_sound) in annotdata:
        if is_sound:
            sound_mask[int(framerate*from_t):int(framerate*to_t)] = True


    fig = plt.figure()
    print([l for l in dir(fig) if "title" in l])
    fig.suptitle("Wave form and intensity distribution of '{}'".format(
                                        os.path.basename(speechfile)))

    #top plot: wave form
    ax = fig.add_subplot(211)
    (ymin, ymax) = (-1.0, 1.0)
    ax.plot(inds, data)
    sounds = collections.BrokenBarHCollection.span_where(
        inds, ymin=ymin, ymax=ymax, where=sound_mask, facecolor='green', alpha=0.2)
    ax.add_collection(sounds)
    pauses = collections.BrokenBarHCollection.span_where(
        inds, ymin=ymin, ymax=ymax, where=1 - sound_mask, facecolor='gray', alpha=0.05)
    ax.add_collection(pauses)
    ax.set_ylim([ymin, ymax])
    ax.set_xlim([0, tmax])
    ax.set_xlabel('time')
    ax.set_ylabel('')

    #bottom plot: intensity distribution
    ax = fig.add_subplot(212)

    sds = data[sound_mask]
    stat_array(sds, "sound spl")
    sils = data[np.invert(sound_mask)]
    stat_array(sils, "silence spl")

    sds_int = compute_intensity(sds, framerate, min_pitch=190)
    stat_array(sds_int, "sound intensity")
    sil_int = compute_intensity(sils, framerate, min_pitch=190)
    stat_array(sil_int, "silence intensity")

    sds_int_log = np.log(sds_int)
    stat_array(sds_int_log, "sound intensity (log)")
    sil_int_log = np.log(sil_int)
    stat_array(sil_int_log, "silence intensity (log)")

    ax.hist(sds_int_log, bins=100, facecolor='green', alpha=0.3)
    ax.hist(sil_int_log, bins=100, facecolor='gray', alpha=0.3)
    ax.set_xlabel('log(intensity)')
    ax.set_ylabel('#samples')


#    plt.show()
#    input()
    plt.savefig(outfile)

def stat_array(x, heading=""):
    print("{}: type={}, N={}, sum={}, mean={}, min={}, max={}".format(
        heading, x.dtype, len(x), x.sum(), x.mean(), x.min(), x.max()))

def compute_intensity(data, framerate, min_pitch=90):
    points = int(framerate * 3.2/min_pitch)
    data_sq = np.power(data, 2)
    w = np.kaiser(points, 20.0)
    intensity = np.convolve(data_sq, w/w.sum(), 'valid')
    return intensity


def parsepraat(infile):
    with open(infile) as f:
        lines = f.readlines()
    lines = lines[12:] #metadata cruft
    def is_sound(s):
        return s.strip() not in ['""', '"xxx"']
    annots = [(float(f), float(t), is_sound(sd)) for (f, t, sd)
               in zip(lines[0::3], lines[1::3], lines[2::3])]
    return annots


def main():
    infile = sys.argv[1]
    annotfile = sys.argv[2]
    outfile = os.path.basename(infile).replace(".wav", ".pdf")
    annotdata = parsepraat(annotfile)
    if infile != outfile:
        plot_wave_spec(infile, outfile, annotdata)

main()
