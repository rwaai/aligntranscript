#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals


import logging, os.path, subprocess, sys, wave, time
import numpy as np
import scipy.io.wavfile as siw
#read, write: http://docs.scipy.org/doc/scipy-0.11.0/reference/tutorial/io.html

from config import CFG

def wavread(f):
    (sf, data) = siw.read(f)
    return (data, sf)

def wavwrite(filename, sf, data):
    logging.debug("read audio ({} samples, dtype={}, sf={}, min={}, max={}, mean={}) from file '{}'".format(
            len(data),        data.dtype, sf, data.min(), data.max(), data.mean(),      filename))
    siw.write(filename, sf, data.astype(np.int16))


def wavplay(filename, chunksize=1024, p=CFG.pyaudio):
    if p is None:
        return
    wf = wave.open(filename, 'rb')
    (nchannels, sampwidth, framerate, _nframes, _comprt, _comprn) = wf.getparams()
    stream = p.open(
        format=p.get_format_from_width(sampwidth),
        channels=nchannels,
        rate=framerate,
        output=True)

    chunk = wf.readframes(chunksize)
    while chunk:
        stream.write(chunk)
        chunk = wf.readframes(chunksize)

# unfortunately, the nicer-looking
#    for chunk in iter(lambda: wf.readframes(chunksize), ''):
#        stream.write(chunk)
# gives warnings with python2 with '' as sentinel,
# whereas with b'' as sentinel it does not work at all with python3

    time.sleep(0.5)          #in this application, it is crucial that
    stream.stop_stream()     #the sound will not be chopped off
    stream.close()

def resample(filename,
             newfile=None,
             target_sf=CFG.audio_resample_freq, #8000 or 16000, usually
             bits=CFG.audio_resample_bitdepth,
             monoize=False):
    if newfile is None:
        (path, sdext) = os.path.splitext(filename)
        (path0, _oldparam) = os.path.splitext(path)
        newparam = ".{}b{}hz".format(bits, target_sf)
        newfile = path0 + newparam + sdext

    args = [CFG.sox_path, filename]
    args.extend(["-b", str(bits)])           #resampling bit depth
    args.extend([newfile])                   #new file name
    args.extend(["rate", str(target_sf)])    #resampling frequency
    if monoize:
        args.extend(["channels", "1"])       #average channels
    subprocess.check_output(args)
    return newfile


def main():
    infile = sys.argv[1]
    if not infile.endswith(".wav"):
        raise SystemExit("give me a *.wav")
    resampled = infile.replace(".wav", ".resampled.wav")
    if resampled != infile:
        resample(infile, newfile=resampled)
        wavplay(resampled)
    copy = infile.replace(".wav", ".copy.wav")
    if copy != infile:
        (data, sf) = wavread(infile)
        wavwrite(copy, data, sf)
        wavplay(copy)

if __name__ == '__main__':
    main()
