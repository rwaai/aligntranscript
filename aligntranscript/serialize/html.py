#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

from codecs import open
from collections import namedtuple
import datetime, logging, os.path


from config import CFG
from utils.clicolor import printok
HtmlChunk = namedtuple("HtmlChunk", ["label", "fromsec", "tosec", "cand"])


#annots :: [(lineno, line, [(label, fromsec, tosec, cand)])]
#labels ::
#htmloutput.write_html(result, labels, sound, htmldir)

def write_html(annots, labels, sound, output_dir=CFG.html_output_dir):
    logging.debug("writing html to {}...".format(output_dir))
    html = make_html(annots, labels, sound, output_dir)
    outfile = os.path.join(output_dir, "index.html")
    with open(outfile, "w", encoding="utf-8") as f:
        f.write(html)
    printok("wrote html with linked sound excerpts to '{}'".format(outfile))

def audio_filename(sound, fromsec, tosec, label="_", output_dir=CFG.html_output_dir):
    from_msec = int(1000 * fromsec)
    to_msec = int(1000 * tosec)
    out_base = "{:s}-{:06d}-{:06d}.wav".format(label, from_msec, to_msec)
    out_path = os.path.join(output_dir, out_base)
    return (out_path, out_base)


def make_html(annots, labels, sound, output_dir, output_view=CFG.html_output_view):
    if output_view == "list":
        (headerf, bodyf) = (tr_div_headers, div_annotations)
    elif output_view == "table":
        (headerf, bodyf) = (tr_headers, tr_annotations)
    else:
        raise ValueError("unknown html view: {}".format(output_view))
    headers_html = headerf(labels)
    annotations_html = []
    for (lineid, line, chunkcands) in annots:
        links = []
        for row in chunkcands:
            ch = HtmlChunk(*row)
            (abslink, rellink) = audio_filename(
                sound, ch.fromsec, ch.tosec, ch.label, output_dir)
            sd = sound.from_slice(ch.fromsec, ch.tosec)
            sd.write(abslink)
            links.append((rellink, ch))
        annotations_html.append(bodyf(lineid, line, links))

    html = html_template(annotations_html, headers_html)
    return html

########## HTML BOILERPLATE BELOW

def tr_headers(labels):
    headers = ["<th>{} <br /> c{:0.2f} <br />  p{:02d} <br /> t{:0.3e}</th>".format(*labelcostpercthr)
        for labelcostpercthr in labels]
    return  "<tr><th>#</th><th>line</th>{}</tr>".format(" ".join(headers))

def tr_annotations(lineid, line, links):
    sounds = [ "<td class='{}'>{} <span class='time'>{:0.2f}-{:0.2f}</span></td>".format(
            c.cand, audiolink(link), c.fromsec, c.tosec) for (link,c) in links]
    return  "<tr><td>{}</td><td>{}</td>{}</tr>".format(
        lineid, line, " ".join(sounds))

def tr_div_headers(_labels):
    return  "<tr><th>#</th><th>line</th><th>chunk candidates</th></tr>"

def div_annotations(lineid, line, links):
    sounds = [ "<div class='{}'>{} {}:<span class='time'>{:0.2f}-{:0.2f}</span></div>".format(
            ch.cand, audiolink(link), ch.label, ch.fromsec, ch.tosec) for (link,ch) in links if ch.cand != 'hide']
    return  "<tr><td>{}</td><td>{}</td><td class='chunklist'>{}</td></tr>".format(
        lineid, line, " ".join(sounds))


def audiolink(url):
    return '<audio controls><source src="{}" type="audio/wav"></audio>'.format(url)

def timestamp_now():
    return datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S")

def html_template(annotations_html, headers_html):
    fields = dict()
    fields['date'] = "{}".format(timestamp_now())
    fields['annotations'] = "\n".join(annotations_html)
    fields['annotation_count'] = len(annotations_html)
    fields['table_headers'] = headers_html
    fields['audio_file'] = CFG.audio_file
    fields['audio_file_abs'] = os.path.abspath(CFG.audio_file)
    fields['transcript_file'] = CFG.transcript_file

    return \
"""\
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>DP aligner 0.01</title>
<style>
td {{vertical-align:middle;}}
.transcript {{width:50%;}}
.inspect {{background-color:red;}}
.adjust {{background-color:yellow;}}
div {{display: inline; border:1px solid #ffffff; margin:3px; height:30px}}
div.hide {{display:none;}}
td.chunklist div {{border:1px solid black;}}
.confirmed {{background-color:green;}}
.discard {{background-color:blue;}}
.audio {{font-size: xsmall;}}
.time {{font-size:60%;}}

tr:nth-child(even) {{background: #CCC}}
tr:nth-child(odd) {{background: #EEE}}
audio {{ width: 40px; margin:0px; }}
</style>
</head>
<body>
<h1>Interim report of DP transcription aligner</h1>
<pre>
date: {date}
audiofile: {audio_file_abs}
transcriptfile: {transcript_file}
#transcript chunks: {annotation_count}
</pre>
<table>
{table_headers}
{annotations}
</table>
</body>


</html>
""".format(**fields)

