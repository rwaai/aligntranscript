#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

from codecs import open
from operator import itemgetter
import logging, os.path, sys
from config import CFG
from utils.clicolor import printok

#annots :: [(lineno, line, [(label, fromsec, tosec, cand)])]
#htmloutput.write_html(result, labels, sound, htmldir)

def write_elan(annots,
               elan_validate=CFG.elan_validate,
               eaffile=CFG.elan_output_file):
    logging.debug("writing elan eaf file...")
    xml = make_elan(annots)
    with open(eaffile, "w", encoding="utf-8") as f:
        f.write(xml)
    printok("wrote elan file to '{}'".format(eaffile))
    if elan_validate:
        if not CFG.elan_validator:
            CFG.elan_validator = make_validator(CFG.elan_schema_file)
        CFG.elan_validator(eaffile)
        printok("validated elan file '{}'".format(eaffile))



def make_elan(timed_annots, metadata=None):

    def tss_annots(timed_annots):
        tss = {}
        annots = []
        (next_ts_id, next_annot_id) = (1, 1)
        for (line, fromsec, tosec) in timed_annots:
            start = int(1000 * fromsec) #to milliseconds
            if start not in tss:
                tss[start] = next_ts_id
                next_ts_id += 1
            end = int(1000 * tosec) #to milliseconds
            if end not in tss:
                tss[end] = next_ts_id
                next_ts_id += 1
            annots.append((next_annot_id, tss[start], tss[end], line))
            next_annot_id += 1
        tss = [(ts_id, ms) for (ms, ts_id) in sorted(tss.items(), key=itemgetter(1))]
        return (tss, annots)

    (tss, annots) = tss_annots(timed_annots)
    time_slots = "\n".join([time_slot_xml(*x) for x in tss])
    annotations = "\n".join([annotation_xml(*x) for x in annots])
    annotation_ct = len(annots)
    xml = elan_template(time_slots, annotations, annotation_ct, metadata)
    return xml


def make_validator(xml_schema_file):
    try:
        from lxml import etree
        xml_schema_doc  = etree.parse(xml_schema_file)
        xml_schema = etree.XMLSchema(xml_schema_doc)

    except ImportError as e:
        logging.fatal("%s \n"
                      "You asked for validation, but this requires lxml (http://lxml.de) "
                      "Either install lxml or run without validation",
                      e)
        sys.exit(1)
    except TypeError as e:
        logging.fatal("You asked for validation, but this requires an xml schema file, e.g. "
                      "http://www.mpi.nl/tools/elan/EAFv2.7.xsd ")
        sys.exit(1)
    except IOError as e:
        logging.fatal(e)
        sys.exit(1)

    def validator(sessionfile):
        with open(sessionfile, encoding="utf8") as f:
            session = etree.parse(f)
        try:
            xml_schema.assertValid(session)
        except etree.DocumentInvalid as e:
            logging.warning("xml validation error: ".format(e))

    return validator

########## XML BOILERPLATE BELOW
def time_slot_xml(ts_id, ms, indent=2):
    return ' ' * indent * 4 + '<TIME_SLOT TIME_SLOT_ID="ts%d" TIME_VALUE="%d"/>' % (ts_id, ms)

def annotation_xml(annot_id, start_id, end_id, annot):
    return \
'''
        <ANNOTATION>
            <ALIGNABLE_ANNOTATION ANNOTATION_ID="a%d" TIME_SLOT_REF1="ts%d" TIME_SLOT_REF2="ts%d">
                <ANNOTATION_VALUE>%s</ANNOTATION_VALUE>
            </ALIGNABLE_ANNOTATION>
        </ANNOTATION>
''' % (annot_id, start_id, end_id, annot)

def timestamp_now():
    import datetime
    return datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S%Z")

def elan_template(time_slots, annotations, annotation_ct, metadata):
    if metadata is None:
        metadata = dict()
    metadata['sound_file'] = CFG.audio_file
    abssound = os.path.abspath(metadata['sound_file'])
    args = {}
    args['mime_type'] = metadata.get('mime_type', "audio/x-wav")
    args['author'] = metadata.get('author', 'unspecified')
    args['date'] = metadata.get('date', "%s" % timestamp_now())
    args['extracted_from'] = abssound
    args['media_url']      = abssound
    args['relative_media_url'] = os.path.relpath(abssound)
    args['time_slots'] = time_slots
    args['annotations'] = annotations
    args['last_used_id'] = annotation_ct
    return \
"""\
<?xml version="1.0" encoding="UTF-8"?>
<ANNOTATION_DOCUMENT
  AUTHOR="{author}"
  DATE="{date}"
  FORMAT="2.7" VERSION="2.7"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:noNamespaceSchemaLocation="http://www.mpi.nl/tools/elan/EAFv2.7.xsd">

  <HEADER
  MEDIA_FILE=""
  TIME_UNITS="milliseconds">

  <MEDIA_DESCRIPTOR
  EXTRACTED_FROM="file://{extracted_from}"
  MEDIA_URL="file://{media_url}"
  MIME_TYPE="{mime_type}" RELATIVE_MEDIA_URL="./{relative_media_url}"/>

  <PROPERTY NAME="lastUsedAnnotationId">{last_used_id}</PROPERTY>

   </HEADER>

    <TIME_ORDER>
{time_slots}
    </TIME_ORDER>
    <TIER DEFAULT_LOCALE="us" LINGUISTIC_TYPE_REF="default" TIER_ID="Tier-0">
{annotations}
    </TIER>
    <LINGUISTIC_TYPE GRAPHIC_REFERENCES="false" LINGUISTIC_TYPE_ID="default" TIME_ALIGNABLE="true"/>
    <LOCALE COUNTRY_CODE="EN" LANGUAGE_CODE="us"/>
    <CONSTRAINT DESCRIPTION="Time subdivision of parent annotation's time interval, no time gaps allowed within this interval" STEREOTYPE="Time_Subdivision"/>
    <CONSTRAINT DESCRIPTION="Symbolic subdivision of a parent annotation. Annotations refering to the same parent are ordered" STEREOTYPE="Symbolic_Subdivision"/>
    <CONSTRAINT DESCRIPTION="1-1 association with a parent annotation" STEREOTYPE="Symbolic_Association"/>
    <CONSTRAINT DESCRIPTION="Time alignable annotations within the parent annotation's time interval, gaps are allowed" STEREOTYPE="Included_In"/>
</ANNOTATION_DOCUMENT>
""".format(**args)

