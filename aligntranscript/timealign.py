#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import logging, sys, traceback, sndhdr
import pprint

from config import CFG
cfg = CFG.setup()


import transcript
import audio, soundio
import chunker
import alignment
import mergealignments, linecol
from utils.clicolor import printerr, printok
from utils.six import input
from plot import plotchunking



def main():
    #read audio; if necessary, first resample and write to new file
    soundfile = CFG.audio_file
    (type, sampl_rate, channels, frames, bitdepth) = sndhdr.what(soundfile)
    if (sampl_rate != CFG.audio_resample_freq or
        bitdepth   != CFG.audio_resample_bitdepth):
        soundfile = soundio.resample(soundfile)
    sd = audio.Audio.read_sound(soundfile)
    done = False
    while not done:
        done = align(sd)

def plotchunks(trans_paras, all_alignments, plot_chunks_file=CFG.plot_chunks_file):
    translens = [("translens", trans_paras[0].relative_lengths()) ]
    plotdata = []
    for a in all_alignments[::1]:
        perc = a[0].chunking.perc
        chunks = a[0].chunking.chunks
        plotdata.append((perc, chunks))
        print("*** PERC", perc, chunks[0], chunks[-1])
    plotchunking.chunkingplot(plotdata,
                              plot_chunks_file,
                              translens,
                              title="Estimated relative utterance lengths")


def align(sd):
    #read transcript
    trans_paras = transcript.Transcript.read_from_file(CFG.transcript_file)
    logging.debug("read transcript:\n{}".format(repr(trans_paras)))
    all_alignments = []
    steps = list(range(CFG.pause_perc_hi, CFG.pause_perc_lo-1, -CFG.pause_perc_step))
    #keeping track of which percentiles which have produced alignments for _any_ para
    #(if none, then that percentile can be suppressed in the interface)
    good_percentiles = [False] * len(steps)
    for (paraid, para) in enumerate(trans_paras):
        logging.info("*** processing transcription para {}/{}...".format(paraid+1, len(trans_paras)))
        assert para
        sdslice = sd.from_slice(para.start, para.end)

        if len(para) > 1:
            #compute audio chunkings, stepping down percentile

            willchunk = chunker.Chunker(sdslice)
            logging.info("computing chunkings...")
            chunkings = [willchunk.get_chunking(perc) for perc in steps]
            maxsize = max(len(c) for c in chunkings if c)
            logging.info("largest chunking is {} elements".format(maxsize))
            if maxsize > CFG.warn_chunks:
                logging.warning("alignment is cubic in largest chunking ({}) -- ".format(maxsize))
                logging.warning("consider giving a few hints in interval {}--{}".format(para.start,para.end))
            alger = alignment.Aligner(para, sdslice)
            logging.info("computing alignments...")
            alignments = [alger.get_alignment(c) if c else None for c in chunkings]
            good_percentiles = [bool(x) or y for (x,y) in zip(alignments, good_percentiles)]
        else: #len(para) == 1
            lpause_start = trans_paras[paraid-1].end if 0 <= paraid-1 else None
            rpause_end = trans_paras[paraid+1].start  if paraid+1 < len(trans_paras) else None
            ch = audio.Chunk.from_sound(sdslice, para[0].start, para[0].end, lpause_start, rpause_end)
            alignments = [alignment.Alignment(cost=0.0, transcript=para,
                                              chunking=chunker.Chunking.singleton(ch), aligned=[ch])] * len(steps)

        all_alignments.append(alignments)

    #transpose: [para [percentile]] --> [percentile [para]]
    all_alignments = list(reversed(list(zip(*all_alignments))))
    assert len(all_alignments) == len(good_percentiles)
    #remove percentiles which failed to produce any alignment (not strictly necessary)
    if not all(p.is_finished() for p in trans_paras):
       while not good_percentiles[-1]:
           good_percentiles.pop()
           all_alignments.pop()
       while not good_percentiles[0]:
           good_percentiles.pop(0)
           all_alignments.pop(0)

    if CFG.plot_chunks:
        plotchunks(trans_paras, all_alignments)
        logging.info("wrote chunk plot to {}".format(CFG.plot_chunks_file))

    ac = mergealignments.InteractiveAlignmentMerger(sd, trans_paras, all_alignments)
    while True:
        try:
            ac.cmdloop()
        except mergealignments.ForceRecomputationError:
            printok("forcing recomputation...")
            return False
        except (mergealignments.NoArgumentError,
                linecol.IntervalParseError,
                NotImplementedError) as e:
            printerr(e)
            continue
        except KeyboardInterrupt:
            ans = input("to really quit (will lose any changes), enter 'q': ")
            if ans == 'q':
                return True
            else:
                continue
        except Exception as e:
            printerr("[SHOULD NOT HAPPEN -- PLEASE REPORT TO marcus.uneson@ling.lu.se]")
            printerr("exception: {}".format(e))
            tb = sys.exc_info()[2]
            traceback.print_tb(tb)
            continue
        else:
            return True

if __name__ == '__main__':
    main()
