#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import logging

import dpalign
from audio import Chunk

class Alignment(object):
    def __init__(self, transcript, chunking, cost, aligned):
        self.transcript = transcript
        self.chunking = chunking
        self.cost = cost
        self.aligned = aligned
        self.alignment = zip(aligned, transcript)

    def write_pairs(self):
        assert len(self.aligned) == len(self.transcript)
        for (i, (line, chunk)) in enumerate(zip(self.transcript, self.aligned)):
            print(i, chunk, line)

    def __getitem__(self, i):
        return self.aligned[i]

    def __len__(self):
        return len(self.transcript)

    #FIXME
    def __repr__(self):
        return "Alignment(cost={},\ntranscript={},\nchunking={},\naligned={})".format(
            self.cost, self.transcript, self.chunking, self.aligned)
    #FIXME
    def __str__(self):
        return "{}".format(self.alignment)


class Aligner(object):
    def __init__(self, transcript, sound=None):

        self.transcript = transcript
        self.audio_fromsec = sound.fromsec
        self.audio_tosec = sound.tosec
        self.trans_chunks = [
            (None, r) for r in transcript.relative_lengths()]


    def get_alignment(self, chunking):
        audio_chunks = chunking.times_rel
        try:
            (cost, pairings) = dpalign.pair_audio_trans(audio_chunks, self.trans_chunks)
        except dpalign.AlignmentError as e:
            logging.debug("failed alignment for perc={0} (thr={1:0.5e}, {2} audio chunks, {3} trans chunks): {4}".format(
                chunking.perc, chunking.thr, len(audio_chunks), len(self.trans_chunks), e))
            return
        chunks = []
        for (i, (trans_ind, chunk_inds)) in enumerate(pairings):
            group = [chunking.chunks[j] for j in chunk_inds]
            ch0 = group[0]
            chN = group[-1]

            lgroup = [chunking.chunks[j] for j in pairings[i-1][1]] if 0 <= i-1 < len(pairings) else None
            lpause_start = lgroup[-1].tosec if lgroup else self.audio_fromsec

            rgroup = [chunking.chunks[j] for j in pairings[i+1][1]] if 0 <= i+1 < len(pairings) else None
            rpause_end = rgroup[0].fromsec if rgroup else self.audio_tosec

            chunks.append(Chunk(
                ch0.data,
                ch0.sf,
                ch0.fromsec,
                chN.tosec,
                lpause_start=lpause_start,
                rpause_end=rpause_end
                ))

        debug_alignment(cost, chunking.perc, chunking.thr, audio_chunks, self.trans_chunks, pairings, chunks)
        logging.debug("computed alignment for perc={0} (thr={1:0.5e}, {2} audio chunks, {3} trans chunks): cost {4}".format(
            chunking.perc, chunking.thr, len(audio_chunks), len(self.trans_chunks), cost))
        return Alignment(self.transcript, chunking, cost, chunks)


def debug_alignment(cost, perc, threshold, audio_chunks, trans_chunks, pairings, annots):
    logging.debug("*** DEBUG ALIGNMENT")
    logging.debug("cost=%f perc=%d thr=%f %d audio chunks %d trans chunks", cost, perc, threshold, len(audio_chunks), len(trans_chunks))
    logging.debug("AUDIO CHUNKS [%d] (pause, sound):", len(audio_chunks))
    logging.debug("\n"+"\n".join(["(p%3.4f,s%0.4f)" %(p, s) for (p, s) in audio_chunks]))
    logging.debug("TRANSCRIPT CHUNKS [%d] (pause, sound):", len(trans_chunks))
    logging.debug("\n"+"\n".join(["(pause %3.4f,sound %0.4f)" %(s, s) for (p, s) in trans_chunks]))
    logging.debug("PAIRINGS [%d] (trans_ind, chunk_inds):", len(pairings))
    logging.debug("\n"+str(pairings))
    logging.debug("TIMED ANNOTATIONS [%d] ):", len(annots))
    logging.debug("\n"+str(annots))
