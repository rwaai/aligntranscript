#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import logging

try:
    from termcolor import cprint
except ImportError:
    logging.warning("module termcolor not found -- you won't have colored cli")
    def cprint(*args, **kwargs):
        kwargs.pop("color")
        print(*args, **kwargs)

#allowed colors: grey red green yellow blue magenta cyan white
def printerr(s, **kwargs):
    cprint(s, color="red", **kwargs)

def printok(s, **kwargs):
    cprint(s, color="green", **kwargs)

def printwarn(s, **kwargs):
    cprint(s, color="yellow", **kwargs)

def printhl(s, **kwargs):
    cprint(s, color="cyan", **kwargs)
