#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import itertools
from utils.six import letters

# misc
def labels(seq=letters):
    for n in itertools.count(1):
        for s in itertools.product(seq, repeat=n):
            yield ''.join(s)
