#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

#18:32:34.23  -- hms
#32:34.23     -- ms
#34.23        -- s

# time utilities
def hms2secs(s):
    if not s.strip(): return
    hms = s.split(":")
    try:
        secs = float(hms.pop())
        mins = float(hms.pop()) if hms else 0.0
        hours = float(hms.pop()) if hms else 0.0
    except ValueError as e:
        print("unparsable time '{}': {}".format(s, e))
        return
    return 3600*hours + 60*mins + float(secs)

def secs2hms(secs):
    hours = int(secs // 3600)
    secs -= 3600 * hours
    mins = int(secs // 60)
    secs -= 60 * mins
    return (hours, mins, secs)

def secs2hmsstr(secs, precision=2):
    (h, m, s0) = secs2hms(secs) #int, int, float
    secs = ("0" if s0 < 10 else "") + "{:.{precision}f}".format(s0, precision=precision)
    if h == 0 and m == 0:
        return secs
    elif h == 0:
        return "{:02d}:{}".format(m, secs)
    else:
        return "{}:{:02d}:{}".format(h, m, secs)

