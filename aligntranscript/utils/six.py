#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import string, sys

# cross-version
PY3 = sys.version > '3'

letters = string.ascii_letters if PY3 else string.letters
input = input if PY3 else raw_input
