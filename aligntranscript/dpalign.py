#!/usr/bin/python3
# -*- coding: utf-8 -*-
from __future__ import division, print_function, unicode_literals

import logging, math


def fill_matrix(rows, cols, value):
    return [[value]*cols for _ in range(rows)]


#a bit like standard levenshtein, except (by assumption)
#one or more tokens from seq1 will always be paired with a single token from seq2
#i.e.: no insertion, no deletion, but variable-length substitution
def levenshtein_substar(seq1, seq2, cost_f):
    m, n = len(seq1), len(seq2)
    costs = fill_matrix(m+1, n+1, value=None)
    for i in range(0, m+1):
        costs[i][0] = (float("inf"), None)
    for j in range(0, n+1):
        costs[0][j] = (float("inf"), None)

    costs[0][0] = (0, None)
    #fill matrix
    for i in range(1, m+1):
        for j in range(1, n+1):
            delt = []
            for k in range(1, i+1):
                prevcost = costs[i-k][j-1][0]
                thiscost = cost_f(seq1, i-k, i, seq2, j)
                delt.append( (prevcost + thiscost, i-k, k) )
            costs[i][j] = min(delt) #(cost, where from, number taken)
    debug_cost_matrix(costs)
    cost_pairings = read_path(costs)
    return cost_pairings

def debug_cost_matrix(costs):
    debug_matrix = []
    m = len(costs)-1
    n = len(costs[0])-1
    debug_matrix.append(" " * 10)
    for j in range(1, n+1):
        debug_matrix.append("trseg%-7d " % j)
    debug_matrix.append("\n")
    for i in range(1, m+1):
        debug_matrix.append("sdseg%-2d|" % i)
        for j in range(1, n+1):
            (cost, wherefrom, take) = costs[i][j]
            debug_matrix.append("%6.3f %2d %2d " % (cost, wherefrom, take))
        debug_matrix.append("\n")
    logging.debug("cost matrix\n")
    logging.debug("".join(debug_matrix))

class AlignmentError(ValueError):
    pass

def read_path(costs):
    sound_seg = len(costs)-1
    transcript_seg = len(costs[0])-1
    pairings = []
    cost = costs[sound_seg][transcript_seg][0]
    while transcript_seg > 0:
        prev_sound_seg = costs[sound_seg][transcript_seg][1]
        if prev_sound_seg is None:
            raise AlignmentError("no segmentation at {}, {}".format(sound_seg, transcript_seg))
        sounds = list(range(prev_sound_seg, sound_seg))
        transcript_seg -= 1
        pairings.append((transcript_seg, sounds ))
        sound_seg = prev_sound_seg
    pairings.reverse()
    return (cost, pairings)


#how useful this segmentation is can be estimated by
#a) the total length of assumed sounds in seq1[from1..to1]
#   (closer to estimate is better)
#b) the length of the pause at seq1[from1] (bigger is better)
#c) the total length of internal pauses (pauses in assumed sounds in seq1[from1..to1])
#   count away from estimate (count on worst case)

def lsh_substar_cost(seq1, from1, to1, seq2, at2, good_pause=0.5, pause_wt=0.5):
    combine = lambda x: math.log(x, 2)
    #seq1 is a list of relative durations: [(pause-rel-dur, sound-rel-dur)]
    #seq2 also, but pauses are ignored: [(None, transcript-rel-dur)]
    target = seq2[at2-1][1]
    (_pause0, sd0) = seq1[to1-1]
    (pause, _sd) = seq1[from1]
    pause = pause or 0.01 #!! FIXME

    allsds = sum(sd for (pause, sd) in seq1[from1:to1-1])
    internal_pauses = sum(pause for (pause, sd) in seq1[from1+1:to1-1])
    est = sd0 + allsds
    est += internal_pauses * (1 if est > target else -1) #choose worst-case
    if est < 0:
        cost = 100# float("inf")
    elif abs(est - target) < 0.00001:
        cost = 0
    elif target <= 0:
        cost = 100#float("inf")
    else:
        cost = abs(combine(est / target))
    if not (pause is None or pause > 0) and good_pause > 0:
        raise AlignmentError("no segmentation: pause={}".format(pause))
    cost += pause_wt*abs(combine(min(pause, good_pause) / good_pause))
    return cost

#main entry point
def pair_audio_trans(audio_chunks, trans_chunks):
    if audio_chunks[-1][1] == 0:
        logging.debug("popping empty trailing sound") #!! invariant??
        audio_chunks.pop()
    logging.debug("aligning {} audio chunks x {} transcription chunks".format(
        len(audio_chunks), len(trans_chunks)))
    cost_pairings = levenshtein_substar(audio_chunks, trans_chunks, lsh_substar_cost)
    logging.debug("aligned {} audio chunks x {} transcription chunks".format(
        len(audio_chunks), len(trans_chunks)))
    return cost_pairings
