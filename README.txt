

1. INTRODUCTION
aligntranscript is an interactive command-line program which helps in
producing time-aligned annotations for audio speech files (e.g., for
praat, http://www.fon.hum.uva.nl/praat/ (TBD),  or
elan, http://tla.mpi.nl/tools/tla-tools/elan), given

a) an existing transcription file with pauses marked (say, by line or
   paragraph breaks)
b) the corresponding audio source file

Probably, a) is part of legacy data you need to handle. That is,
if you start from scratch, you probably don't need this program --
it might be almost as quick, and certainly
less error-prone, to enter the transcriptions simultaneously with
segmenting the audio (as you would normally in do in audio
annotation programs such as praat or elan). However, another
possible use case occurs when working with native speakers for relatively
short jobs; they may not want to learn the full annotation workflow,
but rather prefer to produce simple, unaligned text. Then you might
use aligntranscript to add time codes later.

Aligntranscript was written to produce reasonable attempts at interactive
alignment also for noisy input (we deal with data from field linguists
in the 70's -- there is often more or less constant background noise,
in addition to random bursts of microphone hitting, dogs barking,
humming from the audience, etc).

Aligntranscript is very much work in progress. Expect bugs and
changes.




2. REQUIREMENTS
To install aligntranscript, you need

* python3 (preferably; but should work under python2 if the other
           dependencies are met)

with a few extra libraries, roughly in order of descending urgency:

* numpy      (crucial)

* scipy.io.wavfile (crucial)

* termcolor (for coloured terminal output; it is a bit more than eye candy
             in the UI)
* lxml      (for validation of the output in elan file format (eaf))

* pyaudio   (for command-line playback; without it, you can still write
             to an html page and play through your browser)

* matplotlib/scipy (only if you wish to do plotting)

Also note:
Your input file may have other parameters for {sampling rate,
bit depth, number of channels} than what you give as desired analysis parameters
on the command line (typically 8000 or 16000 Hz; 16 bits; and the channels
averaged into mono). If so, the program will resample the input accordingly,
but only by externally calling sox (http://sox.sourceforge.net). Thus, either
install sox on your system and set the path to it in config.py (e.g. /usr/bin/sox),
or else resample the input file as desired before starting the alignment.



3. TRANSCRIPTION FORMAT AND ANCHORS
The transcription should mark pauses by some designated separator;
the most convenient way (and the default) is perhaps a blank line
between every chunk of speech. If you use another separator, you
have to specify it on the command line (see -h). For instance,

--- begin transcription file
ùa lʌ̀ʌ lɨ̀aŋ tɛ́ɛk káp ñàar séŋ yɔ̀ tèe

báat nì tɛ́ɛk

...
--- end transcription file

For sound files larger than a few minutes, the alignment will be
prohibitively slow and a bit error-prone. However, you can
greatly reduce the search space by providing points of reference,
or anchors, to the program. They will then be treated as given
constraints. For instance:

--- begin transcription file
ùa lʌ̀ʌ lɨ̀aŋ tɛ́ɛk káp ñàar séŋ yɔ̀ tèe

<3.90> báat nì tɛ́ɛk

...
--- end transcription file

This means that the point in time 3.90 (seconds) is contained somewhere
in the pause between the first and the second line. Times can
be given as <63.90> or <1:03.90> (both meaning 1 minute and 3.9 seconds).
Similarly, <3663.90> and <61:03.90> and <1:01:03.90> all refer
to the same point in time.

The <time> anchor can be placed anywhere in the beginning of the record;
if your separator is two newlines, for instance, you can put also put the
anchor on a line of its own, like so:

<3.90>
báat nì tɛ́ɛk

As a rule of thumb, one anchor per minute gives good results.
(It isn't really the duration itself of a chunk which decides how fast
the aligner will run, but rather the number of automatically produced
chunks. The aligner is basically O(n^3) in this number, so whenever you
have a section with more than 100 or so chunks, you will gain much
speed by providing anchors. You will get a warning suggesting you to do so
when a chunking results in too large a number of elements.)



4. PREPARING A TRANSCRIPTION FILE AND RUNNING

Directory DATA/SAMPLE has a 1-minute sound file "sound.wav" with an original
transcription in "transcript-original.txt" and a prepared one in
"transcript-prepared.txt". The latter has been produced from the former my
manual adjustment of whitespace, so that paragraph breaks corresponds to
pauses in the sound. There is also an example of an anchor at <29.00>
(although this short sample could have done without it).

In my experience, preparing a 10-minute file in this way may take 60 minutes
or so in a language you don't understand (it will be much quicker if you do
know the language). For perspective, a time-aligned phonetic transcription
from scratch may take 100 times longer than the actual recorded time.

To test: download, unzip, cd and try

$ ./timealign -h
  for help, or

$ ./timealign DATA/SAMPLE/sound.wav DATA/SAMPLE/transcript-prepared.txt
  for interactive alignment of the sample file above. Once you have
  reached the prompt of interactive interface (IA MODE), try help.

  The most important commands:
        list (or short: l);
        play (p);
        confirm (c);
        help <command>;
        write FORMAT, with FORMAT one of {txt, html, elan, praat}.


Many configuration options can be set in config.py; some of them can
also be set via command-line arguments (see -h)

